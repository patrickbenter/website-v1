import React from 'react';

import styles from './Experience.module.scss';

export default function ExperienceItem(props: any) {
    return(
        <div className={styles.row}>
            <div className={styles.columnHeading}>
                <p className={styles.job}><strong>{props.job}</strong></p>
            <p className={styles.date}>{props.date}</p>
                </div>
            <div className={styles.column}>
                <p className={styles.workTitle}><strong>{props.workTitle}</strong></p>
                <p>
                    {props.description}
                </p>
            </div>
        </div>
    )
}