import React from 'react';
import { InView } from 'react-intersection-observer';
import { motion } from 'framer-motion';

import { MotionContainer } from '../../util/MotionContainer';
import styles from './Skills.module.scss';

function Skills() {

    const list = {
        visible: { 
            opacity: 1,
            transition: {
                when: "beforeChildren",
                staggerChildren: 0.1,
            },
         },
        hidden: { 
            opacity: 0,
            transition: {
                when: "afterChildren",
            }
        },
    }
    
    const item = {
        visible: { opacity: 1, x: 0 },
        hidden: { opacity: 0, x: -100 },
    }

    const skills = ["SQL / SQL Server", "SSIS", "SSRS", "Python", "C#", "Javascript / Typescript", "React", "Docker"];
    const skillListItems = skills.map((skill, index) => 
        <h4 key={index} className={styles.h4List}><motion.li variants={item}>{skill}</motion.li></h4>
    );

    const options = {
        threshold: 0.3,
        triggerOnce: true,
    }

    return(
        <div className="content-container">
            <div className="content">
                <h2>Skills</h2>

                <InView {...options}>
                    {({ inView, ref }) => (
                        <MotionContainer ref={ref}>
                            <motion.ul initial="hidden" animate={inView ? "visible" : "hidden"} variants={list}>
                                {skillListItems}
                            </motion.ul>
                        </MotionContainer>
                    )}
                </InView>
            </div>
        </div>
    )
}

export { Skills };