import React from 'react';

export type Props = {
    children? : React.ReactNode
    inView? : boolean
}

export const MotionContainer = React.forwardRef<any, Props>((props: Props, ref) => (
    // @ts-ignore
    <div ref={ref}>{props.children}</div>
))