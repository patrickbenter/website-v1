import React, { useState } from 'react';
import { motion } from 'framer-motion';

import styles from './Projects.module.scss';

import { 
    appInput, appInputSrcSet,
    appMain, appMainSrcSet,
    appSettings, appSettingsSrcSet,
    website, websiteSrcSet,
} from '../../util/Images';

function Projects() {

    const [state, setState] = useState({
        x1: 0,
        x2: -50,
        x3: 0,

        z1: 1,
        z2: 2,
        z3: 0,

        s1: 0,
        s2: 1,
        s3: 2,

        animIndex: 0,
    });

    const order = [0.9, 1.0, 0.9];
    
    function carousel() {
        setState({
            x1: (state.x1 + 50) % 150,
            x2: (state.x2 + 150) % 150 - 100,
            x3: (state.x3 + 150) % 150 - 100,

            z1: (state.z1 + 1) % 3,
            z2: (state.z2 + 1) % 3,
            z3: (state.z3 + 1) % 3,

            s1: (state.s1 + 1) % 3,
            s2: (state.s2 + 1) % 3,
            s3: (state.s3 + 1) % 3,
            animIndex: (state.animIndex + 1) % 3,
        });
    };

    const transition = {
        ease: "backInOut",
        duration: 1,
        zIndex: {
            delay: 0.5,
        }
    }
    
    return(
        <div className="content-container">
            <div className="content">
                <h2>Projects</h2>

                <div className={styles.container}>
                    <div className={[styles.half, styles.app].join(' ')}>
                        <motion.div onTapStart={() => carousel()} animate={{y: [-15, 15]}} transition={{ yoyo: Infinity, from: 0, duration: 1.5, ease: "circIn" }}>
                            <motion.img 
                                alt="Settings page of KJ Tracker App"
                                animate={{x: state.x1 + "%", zIndex: state.z1, scale: order[state.s1]}} 
                                transition={transition} 
                                className={styles.appImageLeft} 
                                src={appSettings[appSettings.length - 1].src} 
                                srcSet={appSettingsSrcSet}
                                sizes={"(max-width: 720px) 35vw, 20vw"}
                            />

                            <motion.img 
                                alt="Main page of KJ Tracker App"
                                animate={{x: state.x2 + "%", zIndex: state.z2, scale: order[state.s2]}} 
                                transition={transition} 
                                className={styles.appImageMiddle} 
                                src={appMain[appMain.length - 1].src} 
                                srcSet={appMainSrcSet}
                                sizes={"(max-width: 720px) 35vw, 20vw"}
                            />

                            <motion.img 
                                alt="Input overlay of KJ Tracker App"
                                animate={{x: state.x3 + "%", zIndex: state.z3, scale: order[state.s3]}} 
                                transition={transition} 
                                className={styles.appImageRight} 
                                src={appInput[appInput.length - 1].src} 
                                srcSet={appInputSrcSet}
                                sizes={"(max-width: 720px) 35vw, 20vw"}
                            />
                        </motion.div>
                    </div>
 
                    <div className={[styles.half, styles.text].join(' ')}>
                        <h3>KJ Tracking App</h3>
                        <p>
                            Android app to quickly track KJ intake, show trends and backup user
                            data into a cloud database. Built with Flutter and Google Firestore 
                            in Android Studio. <a href="https://gitlab.com/dopfer/snacktrack" target="_blank" rel="noopener noreferrer">Source Code on GitLab.</a>
                        </p>
                    </div>
                </div>
                
                <div className={styles.container}>
                    <div className={styles.half}>
                        <h3>Portfolio Website</h3>
                        <p>The website that you're currently looking at. Built with React, Typescript 
                            and Framer Motion. Created with responsive mobile design. 
                            Hosted on Gitlab Pages. <a href="https://gitlab.com/dopfer/website" target="_blank" rel="noopener noreferrer">Source Code on GitLab.</a>
                        </p>
                    </div>

                    <div className={styles.half}>
                        <motion.img 
                            alt="Website parallax title region"
                            whileHover={{ scale: 1.05 }}
                            className={styles.websiteImage} 
                            src={website[website.length - 1].src}
                            srcSet={websiteSrcSet}
                            sizes={"(max-width: 720px) 80vw, 30vw"}
                        />
                    </div>
                </div>
            </div>
        </div>
    )
}

export { Projects };
