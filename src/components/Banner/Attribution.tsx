import React, { useState } from 'react';
import { Modal } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faImages } from '@fortawesome/free-solid-svg-icons';

import styles from './Banner.module.scss';

export const Attribution = () => {

    const [state, setState] = useState({
        open: false
    });

    function openModal() {
        setState({ open: true })
    };
    function closeModal() {
        setState({ open: false })
    };

    return(
        <div className={styles.attribution}>
            <FontAwesomeIcon icon={faImages} onClick={openModal} size="1x" />

            <Modal show={state.open} onHide={closeModal}>
                <Modal.Header closeButton>
                    <Modal.Title>Mountain Background Vector</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <a href="https://www.freepik.com/free-photos-vectors/background">Background vector created by freepik - www.freepik.com</a>
                </Modal.Body>
            </Modal>
        </div>
    )
}