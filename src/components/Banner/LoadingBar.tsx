import React, { useState } from 'react';
import SyncLoader from 'react-spinners/SyncLoader';
import { css } from '@emotion/core';

import styles from './Banner.module.scss';

export const LoadingBar = () => {

    const [state] = useState({
        loading: true
    });

    const override = css`
        display: block;
        margin: 0 auto;
    `;

    return(
        <div className={styles.loadingContainer}>
            <SyncLoader css={override} size={30} margin={20} color={"salmon"} loading={state.loading} />
        </div>
    )
}