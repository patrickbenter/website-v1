import React from 'react';
import rellax from 'rellax';

import { Parallax } from './Parallax';
import { LoadingBar } from './LoadingBar';
import { JumpDownArrow } from './JumpDownArrow';
import { Attribution } from './Attribution';
import { parallax } from '../../util/Images';

import styles from './Banner.module.scss';

function Banner() {

    function isMobile(speed: string) {
        return window.innerWidth < 720 ? "0" : speed;
    };

    const parallaxLayers = parallax.map((layer, index) => 
        <Parallax as="div" key={index} className={[styles.parallax__layer, rellax].join(' ')} data-rellax-speed={isMobile(layer.parallaxSpeed)} data-rellax-zindex={layer.parallaxZIndex}>
            <div style={{ backgroundImage: "url(" + layer.src + ")", height: "100%"}}></div>
        </Parallax>
    );

    return (
        <div className={styles.parallax}>
            <LoadingBar />

            <div>
                {parallaxLayers}

                <Parallax as="div" className={[styles.parallax__layer, rellax].join(' ')} data-rellax-speed={isMobile("1")}>
                    <div className={styles.title}>
                        <h1>Patrick Benter</h1>
                    </div>
                </Parallax>
                
                <JumpDownArrow />
                <Attribution />
            </div>
        </div>
    )
}

export { Banner };