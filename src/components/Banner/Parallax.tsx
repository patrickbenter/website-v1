import React, { useEffect, useRef } from 'react';
import Rellax from 'rellax';

// @ts-ignore
export const Parallax = ({ children, as: Component = "div", ...props}) => {
    const rellaxRef = useRef(null);
    useEffect(() => {
        // @ts-ignore
        let rellax = new Rellax(rellaxRef.current);
        return () => {
            rellax.destroy();
        };
    }, []);

    return (
        // @ts-ignore
        <Component ref={rellaxRef} {...props}>
            {children}
        </Component>
    )
}